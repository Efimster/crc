use core::cmp;

const BASE:usize = 65521;

pub fn adler32(data:&[u8]) -> u32 {
    let mut a = 1usize;
    let mut b = 0usize;
    let len = data.len();
    let mut m ;
    let mut i = 0;
    while i < len {
        m = cmp::min(len - i, 2654) + i;
        while i < m {
            a += data[i] as usize;
            b += a;
            i += 1;
        }
        a = 15 * (a >> 16) + (a & 0xffff);
        b = 15 * (b >> 16) + (b & 0xffff);

    }
    (((b % BASE) << 16) | (a % BASE)) as u32
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_adler32() {
        let s = "See the zlib source code for a more efficient implementation that requires a fetch and two additions per byte, with the modulo operations deferred with two remainders computed every several thousand bytes, a technique first discovered for Fletcher checksums in 1988. js-adler32 provides a similar optimization, with the addition of a trick that delays computing the \"15\" in 65536 - 65521 so that modulos become faster: it can be shown that ((a >> 16) * 15 + (a & 65535)) % 65521 is equivalent to the naive accumulation";
        assert_eq!(adler32(s.as_bytes()), 0xd096b3bf);
    }
}